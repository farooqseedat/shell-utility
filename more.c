#include<stdio.h>
#include<unistd.h>
#include<stdlib.h>
#define LINELEN 512 

void do_more(FILE* );
int main(int argc,char *argv[])
{	
	if(argc==1)
	{
		printf("This is the help page\n");
		exit(0);
	}
	FILE *fp;
	int i=0;
	while(++i<argc)
	{
	fp=fopen(argv[1],"r");
	if(fp==NULL)
	{
		perror("Cant open file");
		exit(1);
	}
	do_more(fp);
	
	fclose(fp);
	}
	return 0;

}

void do_more(File *fp)
{
	int num_of_lines=0;
	int rv;
	char buffer[LINELEN];
	while(fgets(buffer,LINELEN,fp))
	{
		fputs(buffer,stdout);
		num_of_lines++;
		if(num_of_lines==PAGELEN)
		{
			rv = get_input();
			if(rv==0)
				break;
			else if(rv==1)
				num_of_lines -=PAGELEN;
			else if (rv==2)
				num_of_lines -=1;
			else if (rv==3)
				break;				
		}
	}
}

int get_input()
{
	char c;
	c=getchar();
	if(c=='q')
		return 0;
	if(c==' ')
		return 1;
	if(c=='\n')
		return 2;
	return 0;
}
